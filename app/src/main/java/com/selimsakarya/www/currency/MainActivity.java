package com.selimsakarya.www.currency;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

        TabHost tabHost;

        TextView txtcurrencyUSD;
        TextView txtcurrencyEUR;
        TextView txtcurrencyQuarter;
        TextView txtcurrencyGram;
        TextView txtcurrencyFull;
        Spinner  txtFirstSpinner;
        Spinner  txtSecondSpinner;
        Spinner  goldSpinner;
        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();
        List<String> goldList = new ArrayList<String>();
        ArrayAdapter<String> adapter1;
        ArrayAdapter<String> adapter2;
        ArrayAdapter<String> goldAdapter;
        EditText firstMoney;
        EditText resultMoney;
        EditText goldNumber;
        EditText money;
        String FirstSelected;
        String SecondSelected;
        String goldSelected;


        float miktar;
        float result;
        private static final float EuroToDolar = (float) 1.16;
        private static final float DolarToEuro = (float) 0.86;
        String getCurrencyEUR;
        String getCurrencyUSD;
        float CurrencyEUR;
        float CurrencyUSD;
        float tamAltın;
        float cAltın;
        float gramAltın;
        float goldResult;
        float goldMiktar;

        private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        MobileAds.initialize(this, "ca-app-pub-5677585668534911~7230867731");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        txtcurrencyUSD = findViewById(R.id.USDtxt);
        txtcurrencyEUR = findViewById(R.id.EURtxt);
        txtcurrencyQuarter = findViewById(R.id.currencyQuarter);
        txtcurrencyFull = findViewById(R.id.currencyTam);
        txtcurrencyGram = findViewById(R.id.currencyGram);

        tabHost = findViewById(R.id.tabhost);
        tabHost.setup();
        TabHost.TabSpec tabSpec;

        //Tab 1
        tabSpec = tabHost.newTabSpec("tab1");
        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("KURLAR", null);
        tabHost.addTab(tabSpec);

        //Tab2
        tabSpec = tabHost.newTabSpec("tab2");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("ÇEVİRİCİ", null);
        tabHost.addTab(tabSpec);


        // Connecting API service

        GetDataFromApi getDataFromApi = new GetDataFromApi();
        GetDataFromApiGold getDataFromApiGold = new GetDataFromApiGold();

        try {

            String urlCurrency = "https://api.canlidoviz.com/web/items?marketId=1&type=0";
            String urlGold = "https://api.canlidoviz.com/web/items?marketId=1&type=1";
            getDataFromApi.execute(urlCurrency);
            getDataFromApiGold.execute(urlGold);

        }catch (Exception e){

        }



        // Tabs Font Size

        TextView x = tabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
        x.setTextSize(30);

        TextView y = tabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);
        y.setTextSize(30);




        // TAB 2
        // Spinner


        firstMoney = findViewById(R.id.firstMoney);
        resultMoney = findViewById(R.id.resultMoney);
        resultMoney.setEnabled(false);

        firstMoney.setFocusable(true);

        list1.add("Dolar");
        list1.add("Euro");
        list1.add("Lira");

        txtFirstSpinner = (Spinner) findViewById(R.id.txtFirstSpinner);
        txtSecondSpinner = (Spinner) findViewById(R.id.txtSecondSpinner);

        adapter1 = new ArrayAdapter<String>(this, R.layout.my_spinner, list1);
        adapter1.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        txtFirstSpinner.setAdapter(adapter1);

        adapter2 = new ArrayAdapter<String>(this, R.layout.my_spinner, list2);
        adapter2.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        txtSecondSpinner.setAdapter(adapter2);


        txtFirstSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

               firstMoney.setText("");
               resultMoney.setText("");


               FirstSelected = txtFirstSpinner.getSelectedItem().toString();
                if(FirstSelected.equals("Dolar")){
                    list2.clear();
                    list2.add("Euro");
                    list2.add("Lira");
                    adapter2.notifyDataSetChanged();
                }
                else if(FirstSelected.equals("Euro")){
                    list2.clear();
                    list2.add("Dolar");
                    list2.add("Lira");
                    adapter2.notifyDataSetChanged();
                }
                else if(FirstSelected.equals("Lira")){
                    list2.clear();
                    list2.add("Dolar");
                    list2.add("Euro");
                    adapter2.notifyDataSetChanged();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        txtSecondSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        SecondSelected = txtSecondSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        firstMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {

                    try {

                        miktar = Float.parseFloat(firstMoney.getText().toString());

                    } catch (NumberFormatException e) {

                    }


                    if (FirstSelected.equals("Dolar") && SecondSelected.equals("Euro")) {
                        result = 0;
                        result = miktar * DolarToEuro;
                    } else if (FirstSelected.equals("Dolar") && SecondSelected.equals("Lira")) {
                        result = 0;
                        result = miktar * CurrencyUSD;
                    } else if (FirstSelected.equals("Euro") && SecondSelected.equals("Dolar")) {
                        result = 0;
                        result = miktar * EuroToDolar;
                    } else if (FirstSelected.equals("Euro") && SecondSelected.equals("Lira")) {
                        result = 0;
                        result = miktar * CurrencyEUR;
                    } else if (FirstSelected.equals("Lira") && SecondSelected.equals("Dolar")) {
                        result = 0;
                        result = miktar / CurrencyUSD;
                    } else if (FirstSelected.equals("Lira") && SecondSelected.equals("Euro")) {
                        result = 0;
                        result = miktar / CurrencyEUR;
                    }


                    resultMoney.setText(String.valueOf(result));

                }
                else
                    resultMoney.setText("");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        // Gold Section

        goldSpinner = findViewById(R.id.goldSpinner);
        goldNumber = findViewById(R.id.goldNumber);
        money = findViewById(R.id.money);
        money.setEnabled(false);

        goldList.add("Tam Altın");
        goldList.add("Çeyrek Altın");
        goldList.add("Gram Altın");

        goldAdapter = new ArrayAdapter<String>(this, R.layout.my_spinner, goldList);
        goldAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        goldSpinner.setAdapter(goldAdapter);


        goldSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        goldNumber.setText("");
                        money.setText("");
                        goldSelected = goldSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });




        goldNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                goldNumber.setFocusable(true);
                goldNumber.setFocusableInTouchMode(true);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() > 0) {

                    try {

                        goldMiktar = Float.parseFloat(goldNumber.getText().toString());

                    } catch (NumberFormatException e) {

                    }


                    if(goldSelected.equals("Tam Altın")){
                        goldResult = 0;
                        goldResult = goldMiktar * tamAltın;

                    }
                    else if(goldSelected.equals("Çeyrek Altın")){
                        goldResult = 0;
                        goldResult = goldMiktar * cAltın;
                    }
                    else if(goldSelected.equals("Gram Altın")){
                        goldResult = 0;
                        goldResult = goldMiktar * gramAltın;
                    }


                    money.setText(String.valueOf(goldResult));

                }
                else
                    money.setText("");

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });




    }



    //Getting Euro And Dolar datas from API
    private class GetDataFromApi extends AsyncTask<String, Void, String>{


        @Override
        protected String doInBackground(String... strings) {

            String result = "";
            URL url;
            HttpURLConnection httpURLConnection;

            try {

                url = new URL(strings[0]);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int data = inputStreamReader.read();

                while(data > 0){

                    char character = (char) data;
                    result += character;
                    data = inputStreamReader.read();

                }

            }catch (Exception e){

            }

            return result;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try {



                JSONArray jsonArray = new JSONArray(s);
                JSONObject dolarObject = jsonArray.getJSONObject(0);
                getCurrencyUSD = dolarObject.getString("buyPrice");


                JSONObject euroObject = jsonArray.getJSONObject(1);
                getCurrencyEUR = euroObject.getString("buyPrice");

                getCurrencyUSD = getCurrencyUSD.substring(0,4);
                getCurrencyEUR = getCurrencyEUR.substring(0,4);



                // Parsing strings to float

                CurrencyEUR = Float.parseFloat(getCurrencyEUR);
                CurrencyUSD = Float.parseFloat(getCurrencyUSD);




                   txtcurrencyUSD.setText(getCurrencyUSD);
                   txtcurrencyEUR.setText(getCurrencyEUR);



            } catch (JSONException e) {
                e.printStackTrace();
            }



        }


    }

    // Getting Gold's datas from API
    private class GetDataFromApiGold extends AsyncTask<String, Void, String>{



        @Override
        protected String doInBackground(String... strings) {

            String result ="";
            URL url;
            HttpURLConnection httpURLConnection;

            try {

                url = new URL(strings[0]);
                httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

                int data = inputStreamReader.read();

                while(data > 0){

                    char character = (char) data;
                    result += character;
                    data = inputStreamReader.read();

                }

            }catch (Exception e){

            }

            return result;
        }



        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try {

                JSONArray jsonArray = new JSONArray(s);
                JSONObject quarterObject = jsonArray.getJSONObject(0);
                String currenyQuarter = quarterObject.getString("buyPrice");

                JSONObject gramObject = jsonArray.getJSONObject(4);
                String currenyGram = gramObject.getString("buyPrice");

                JSONObject fullObject = jsonArray.getJSONObject(2);
                String currenyTam = fullObject.getString("buyPrice");


                currenyGram = currenyGram.substring(0,7);
                currenyQuarter = currenyQuarter.substring(0,7);
                currenyTam = currenyTam.substring(0,7);

                txtcurrencyQuarter.setText(currenyQuarter);
                txtcurrencyGram.setText(currenyGram);
                txtcurrencyFull.setText(currenyTam);

                tamAltın = Float.parseFloat(currenyTam);
                cAltın = Float.parseFloat(currenyQuarter);
                gramAltın = Float.parseFloat(currenyGram);



            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


    }




}

